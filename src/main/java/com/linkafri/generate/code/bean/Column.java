package com.linkafri.generate.code.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Rocky on xxxx-xx-xx.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode()
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Column {
    private String name;
    private String typeName;
    private String className;
}
