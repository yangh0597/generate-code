package com.linkafri.generate.code.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Rocky on xxxx-xx-xx.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode()
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BaseBean {
    private String tableName;
    private String tableNameClass;
    private String driverClassName;
    private String pkClassType;
    private String pkJDBCType;
    private String pkName;//数据库列名称
    private String pkClassName;//java名称 首字母小写
    private String pkClassNameFirstUp;//java名称 首字母大写
    private List<String> tableNameList;
    private String className;
    private String classNameStack;//java变量名称
    private String schemaName;
    private String currentIsDelete;//当前逻辑删除字段
    private String currentIsDeleteClass;//当前逻辑删除java名称字段
    private String mapperSuffix;
    private String mapperInterfacePackageName;
    private String mapperXMLPackageName;
    private String mapperInterfaceParentName;
    private String entityPackageName;
    private String entityParent;
    private String author;
    private String idType;
    private List<HashMap<String, String>> delNames;
    private String createTime;
    private String removeEntityPrefix;
    private String curdPackageName;
    private String jinhao = "#";
    private String dols = "$";
    private Set<String> imports = new TreeSet<>();
    private List<Field> fields = new ArrayList<>();

}
