package com.linkafri.generate.code.constants;

import com.linkafri.generate.code.bean.Schema;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rocky on xxxx-xx-xx.
 */
public class Constants {
    public static List<Schema> EXCLUDE_SCHEMA_LIST;

    static {
        EXCLUDE_SCHEMA_LIST = new ArrayList<Schema>();
        EXCLUDE_SCHEMA_LIST.add(new Schema());
        Schema schema = new Schema();
        schema.setName("information_schema");
        Schema schema2 = new Schema();
        schema2.setName("mysql");
        Schema schema3 = new Schema();
        schema3.setName("performance_schema");
        EXCLUDE_SCHEMA_LIST.add(schema);
        EXCLUDE_SCHEMA_LIST.add(schema2);
        EXCLUDE_SCHEMA_LIST.add(schema3);
    }

    public static final String schemaName = "SCHEMA_NAME";



}
