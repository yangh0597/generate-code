package com.linkafri.generate.code.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 启动监听器
 *
 * @author billy 2017年11月14日 下午2:15:56
 */
@Service
@Slf4j
public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {


    @Override
    public void onApplicationEvent(ContextRefreshedEvent evt) {

    }


}
