package com.linkafri.generate.code.bean;

import lombok.Data;

/**
 * Created by Rocky on xxxx-xx-xx.
 */
@Data
public class Field {
    private String name;
    private String type;
    private String jdbcType;
    private String columnName;
}
