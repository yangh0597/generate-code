package com.linkafri.generate.code.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/** Created by Rocky on xxxx-xx-xx. */
@Slf4j
public class Utils {
  public static String firstUpCase(String str) {
    if (StringUtils.isEmpty(str)) {
      return str;
    }
    str = str.substring(0, 1).toUpperCase() + str.substring(1);
    return str;
  }

  public static String firsLowerCase(String str) {
    str = str.substring(0, 1).toLowerCase() + str.substring(1);
    return str;
  }

  public static String transferColumnToField(String str) {
    str = str.toLowerCase();
    String[] columnFields = str.split("_");
    String result = "";
    for (int i = 0; i < columnFields.length; i++) {
      if (i == 0) {
        result += columnFields[i];
      } else {
        result += Utils.firstUpCase(columnFields[i]);
      }
    }
    return result;
  }

  public static String getType(String str) {
    String[] columnFields = str.split("\\.");
    String result = "";
    result += columnFields[columnFields.length - 1];
    return result;
  }

  public static String tableNameToClassName(String str, String removeEntityPrefix) {
    // 去除前缀
    String[] split1 = str.split(removeEntityPrefix);
    String newStr = "";
    if (split1.length > 1) {
      for (int i = 1; i < split1.length; i++) {
        newStr += split1[i];
      }
      str = newStr;
    }

    String[] split = str.split("_");
    String result = "";
    for (int i = 0; i < split.length; i++) {
      if (i == 0) {
        result += Utils.firstUpCase(split[i]);
      } else {
        result += Utils.firstUpCase(split[i]);
      }
    }

    return result;
  }

  public static String getNowTime() {
    Date currentTime = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String dateString = formatter.format(currentTime);
    return dateString;
  }

  public static String pointToUrlStr(String str) {
    String result = str.replaceAll("\\.", "\\/");
    return result;
  }

  public static String getRealJDBCTypr(String jdbcType) {
    jdbcType = jdbcType.toUpperCase();
    switch (jdbcType) {
      case "VARCHAR":
        {
          break;
        }
      case "SERIAL":
        {
          jdbcType = "INTEGER";
          break;
        }
      case "INT":
        {
          jdbcType = "INTEGER";
          break;
        }
      case "TEXT":
        {
          jdbcType = "VARCHAR";
          break;
        }
      case "INT2":
        {
          jdbcType = "INTEGER";
          break;
        }
      case "INT4":
        {
          jdbcType = "INTEGER";
          break;
        }
      case "INT8":
        {
          jdbcType = "BIGINT";
          break;
        }
      case "BIGSERIAL":
        {
          jdbcType = "BIGINT";
          break;
        }
      case "SMALLINT UNSIGNED":
        {
          jdbcType = "BIGINT";
          break;
        }
      case "BPCHAR":
        {
          jdbcType = "VARCHAR";
          break;
        }
      case "FLOAT4":
        {
          jdbcType = "FLOAT";
          break;
        }
      case "FLOAT8":
        {
          jdbcType = "DOUBLE";
          break;
        }
      case "NUMERIC":
        {
          jdbcType = "DECIMAL";
          break;
        }
      case "BOOL":
        {
          jdbcType = "BOOLEAN";
          break;
        }
      case "DATE":
        {
          jdbcType = "BOOLEAN";
          break;
        }
      case "TIME":
        {
          break;
        }
      case "TIMESTAMP":
        {
          break;
        }
      case "CHAR":
        {
          break;
        }
      default:
        {
          log.error(jdbcType + " jdbc type  error");
          break;
        }
    }
    return jdbcType;
  }

  public static boolean deleteDirectory(String dir) {
    // 如果dir不以文件分隔符结尾，自动添加文件分隔符
    if (!dir.endsWith(File.separator)) dir = dir + File.separator;
    File dirFile = new File(dir);
    // 如果dir对应的文件不存在，或者不是一个目录，则退出
    if ((!dirFile.exists()) || (!dirFile.isDirectory())) {
      System.out.println("删除目录失败：" + dir + "不存在！");
      return false;
    }
    boolean flag = true;
    // 删除文件夹中的所有文件包括子目录
    File[] files = dirFile.listFiles();
    for (int i = 0; i < files.length; i++) {
      // 删除子文件
      if (files[i].isFile()) {
        flag = Utils.deleteFile(files[i].getAbsolutePath());
        if (!flag) break;
      }
      // 删除子目录
      else if (files[i].isDirectory()) {
        flag = Utils.deleteDirectory(files[i].getAbsolutePath());
        if (!flag) break;
      }
    }
    if (!flag) {
      System.out.println("删除目录失败！");
      return false;
    }
    // 删除当前目录
    if (dirFile.delete()) {
      System.out.println("删除目录" + dir + "成功！");
      return true;
    } else {
      return false;
    }
  }

  /**
   * 删除文件，可以是文件或文件夹
   *
   * @param fileName 要删除的文件名
   * @return 删除成功返回true，否则返回false
   */
  public static boolean delete(String fileName) {
    File file = new File(fileName);
    if (!file.exists()) {
      System.out.println("删除文件失败:" + fileName + "不存在！");
      return false;
    } else {
      if (file.isFile()) return deleteFile(fileName);
      else return deleteDirectory(fileName);
    }
  }

  /**
   * 删除单个文件
   *
   * @param fileName 要删除的文件的文件名
   * @return 单个文件删除成功返回true，否则返回false
   */
  public static boolean deleteFile(String fileName) {
    File file = new File(fileName);
    // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
    if (file.exists() && file.isFile()) {
      if (file.delete()) {
        System.out.println("删除单个文件" + fileName + "成功！");
        return true;
      } else {
        System.out.println("删除单个文件" + fileName + "失败！");
        return false;
      }
    } else {
      System.out.println("删除单个文件失败：" + fileName + "不存在！");
      return false;
    }
  }

  public static Connection getConnection(
      String url, String user, String pass, String classForName) {
    Connection conn = null;
    try {
      Class.forName(classForName);
      conn = DriverManager.getConnection(url, user, pass);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return conn;
  }
}
