package com.linkafri.generate.code.bean;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.linkafri.generate.commom.bean.AuditEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Rocky on xxxx-xx-xx.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Table extends AuditEntity<Long> {
    private String name;
    private List<Column> columns = new ArrayList<>();
}
