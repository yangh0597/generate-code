package com.linkafri.generate.code.bean;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.linkafri.generate.commom.bean.AuditEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Rocky on xxxx-xx-xx.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Schema extends AuditEntity<Long> implements Serializable {
    private String name;
    private Table table;


}
