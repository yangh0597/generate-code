/*
该代码由 Generator Code 生成的，开发者邮箱yangh0597@gmail.com
This Code is generated by Generator Code and the developer's email address is yangh0597@gmail.com
*/
package ${baseBean.curdPackageName}.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import ${baseBean.curdPackageName}.dao.${baseBean.className}Dao;
import ${baseBean.entityPackageName}.${baseBean.className};
import ${baseBean.curdPackageName}.service.${baseBean.className}Service;
import com.linkafri.generate.commom.bean.SharkCodeMsgEnum;
import com.linkafri.generate.commom.bean.Page;
import com.linkafri.generate.commom.bean.Result;
import com.linkafri.generate.commom.bean.dto.TransformDTO;
import com.linkafri.generate.commom.service.impl.CrudServiceImpl;

/**
 * ${baseBean.className}Service的实现类
 *
 * @author ${baseBean.author}
 * @date ${baseBean.createTime}
 */
@Service
public class ${baseBean.className}ServiceImpl extends CrudServiceImpl<${baseBean.className}Dao, ${baseBean.className}, ${baseBean.idType}>
        implements ${baseBean.className}Service {
    /**
     * 检查待保存的记录的字段是否有null值
     */
    @Override
    protected <T> void checkNull4Create(${baseBean.className} record, Result<T> result) {
        super.checkNull4Create(record, result);
        if (!result.isSuccess()) {
            return;
        }
        if (record == null) {
            TransformDTO.setErrorInfo(result, SharkCodeMsgEnum.VALUE_IS_BLANK.getCode(), "entity must not be null");
        }
    }

    /**
     * 检查待保存的记录的字段是否符合预期的格式
     */
    @Override
    protected <R> void validFieldValue(${baseBean.className} record, Result<R> result) {
        super.validFieldValue(record, result);
        if (!result.isSuccess()) {
            return;
        }
        if (record == null) {
            TransformDTO.setErrorInfo(result, SharkCodeMsgEnum.VALUE_IS_BLANK.getCode(), "entity must not be null");
        }
    }

    /**
     * 检查待保存的实体对象是否已经有存在相同的记录（幂等校验）
     *
     * @return true: 存在相同记录, false: 不存在相同记录
     */
    @Override
    protected <R> boolean checkRecordIfExist4Create(${baseBean.className} record, Result<R> result) {
        return super.checkRecordIfExist4Create(record, result);
    }

    /**
     * 检查待更新的实体对象是否已经有存在相同的记录（幂等校验）
     *
     * @return true: 存在相同记录, false: 不存在相同记录
     */
    @Override
    protected <R> boolean checkRecordIfExist4Update(${baseBean.className} record, Result<R> result) {
        return super.checkRecordIfExist4Update(record, result);
    }

    /**
     * 查询存在的记录，用于"检查待保存的实体对象是否已经有存在相同的记录（幂等校验）"
     *
     * @param record 待保存的实体对象
     * @return 存在的记录
     * @see CrudServiceImpl#checkRecordIfExist4Create(AbstractEntity, Result)
     * @see CrudServiceImpl#checkRecordIfExist4Update(AbstractEntity, Result)
     */
    @Override
    protected <R> List<${baseBean.className}> findExistRecord4CheckRecord(${baseBean.className} record, Result<R> result) {
        return null;
    }

    /**
     * 检查通用查询条件字段是否为空，默认只检查主键id是否为空，防止全表扫描
     */
    @Override
    protected <R> void checkCommonQueryConditionIsAllNull(${baseBean.className} condition, Result<R> result) {
        if (!result.isSuccess()) {
            return;
        }
        if (condition == null || (condition.getId() == null)) {
            TransformDTO.setErrorInfo(result, SharkCodeMsgEnum.VALUE_IS_BLANK.getCode(),
                    "query condition, (id) must not be all null");
        }
    }

    /**
     * 校验分页查询条件字段是否有空值，默认校验了主键id，防止全表扫描，子类去重写
     *
     * @param condition 分页查询条件
     * @param result 分页查询返回结果
     */
    @Override
    protected void checkPageConditionIsAllNull(Page<${baseBean.className}> condition, Page<${baseBean.className}> result) {
        if (condition == null || condition.getParam() == null) {
            result.setErrorCode(SharkCodeMsgEnum.VALUE_IS_BLANK.getCode());
            result.setErrorMsg("query condition, param must not be null");
            result.setSuccess(false);
        }
    }
}
