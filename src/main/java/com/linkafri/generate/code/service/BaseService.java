package com.linkafri.generate.code.service;

import java.sql.SQLException;
import java.util.List;

import com.linkafri.generate.code.bean.BaseBean;
import com.linkafri.generate.code.bean.Schema;
import com.linkafri.generate.code.bean.Table;
import com.linkafri.generate.commom.bean.Result;

/**
 * Created by Rocky on xxxx-xx-xx.
 */
public interface BaseService {

    Result<List<Schema>> getAllSchema() throws SQLException;

    Result<List<Table>> getAllTableNameBySchema(Schema schema);


    Result<Table> getTableBySchemaAndTable(Schema schema, Table table, BaseBean baseBean);

    Result<BaseBean> getTableByBaseBean(BaseBean baseBean);
}
